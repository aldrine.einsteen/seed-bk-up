package in.bitforest.seed;

import javax.naming.NamingException;

public abstract interface SimpleRegistry {
    public abstract Object lookupComponent(String paramString)
            throws NamingException;

    public abstract Model lookupModel(String modelName)
        throws NamingException;

    public abstract ModelMapping lookupModelMapping(String modelMappingName)
            throws NamingException;

    public abstract Flow lookupFlow(String flowName)
            throws NamingException;

    abstract void registerFlow(Flow flow);
}
