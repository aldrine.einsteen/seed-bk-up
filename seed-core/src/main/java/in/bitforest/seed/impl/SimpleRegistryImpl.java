package in.bitforest.seed.impl;

import in.bitforest.seed.Flow;
import in.bitforest.seed.Model;
import in.bitforest.seed.ModelMapping;
import in.bitforest.seed.SimpleRegistry;

import javax.naming.NamingException;
import java.util.HashMap;
import java.util.Map;

public class SimpleRegistryImpl implements SimpleRegistry {

    private Map registry = new HashMap();

    static {
    }

    @Override
    public Object lookupComponent(String paramString) throws NamingException {
        return null;
    }

    @Override
    public Model lookupModel(String modelName) throws NamingException {
        return null;
    }

    @Override
    public ModelMapping lookupModelMapping(String modelMappingName) throws NamingException {
        return null;
    }

    @Override
    public Flow lookupFlow(String flowName) throws NamingException {
        return null;
    }

    @Override
    public void registerFlow(Flow flow) {

    }
}
