package in.bitforest.seed.impl;

import in.bitforest.seed.*;

import java.rmi.RemoteException;
import java.util.List;

public class FlowImpl implements Flow {
    @Override
    public Object[] run(Object[] arrayOfInputs, ExecutionContext executionContext) throws ExecutionException, RemoteException {
        return new Object[0];
    }

    @Override
    public ExecutionResult<Object[]> run0(Object[] objects, ExecutionContext executionContext) throws RemoteException {
        return null;
    }

    @Override
    public Object[] run(AbstractInputStream inputStream, ExecutionContext executionContext) throws ExecutionException, RemoteException {
        return new Object[0];
    }

    @Override
    public ModelObject run(ModelObject modelObject, ExecutionContext executionContext) throws ExecutionException, RemoteException {
        return null;
    }

    @Override
    public ExecutionResult<ModelObject> run0(ModelObject paramDataObject, ExecutionContext paramTransformContext) throws RemoteException {
        return null;
    }

    @Override
    public ModelObject createInputDataObject() throws RemoteException {
        return null;
    }

    @Override
    public ModelObject createOutputDataObject() throws RemoteException {
        return null;
    }

    @Override
    public List getOutputPorts() throws RemoteException {
        return null;
    }

    @Override
    public void setName(String name) {

    }

    @Override
    public String getName() {
        return null;
    }
}
