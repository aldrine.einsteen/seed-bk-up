package in.bitforest.seed;

import java.io.InputStream;
import java.io.Serializable;

public abstract interface AbstractInputStream extends Serializable {

    public abstract InputStream getAsStream()
            throws ExecutionException;

    public abstract byte[] getAsBytes()
            throws ExecutionException;

    public abstract String getName();
}
