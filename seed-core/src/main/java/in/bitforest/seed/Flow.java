package in.bitforest.seed;

import java.rmi.RemoteException;
import java.util.List;

public interface Flow extends ServiceElement {

    enum Transaction {
        INHERIT, NOTREQUIRED, NEW
    }

    Object[] run(Object[] arrayOfInputs, ExecutionContext executionContext)
            throws ExecutionException, RemoteException;

    ExecutionResult<Object[]> run0(Object[] objects, ExecutionContext executionContext)
            throws RemoteException;

    Object[] run(AbstractInputStream inputStream, ExecutionContext executionContext)
            throws ExecutionException, RemoteException;

    ModelObject run(ModelObject modelObject, ExecutionContext executionContext)
            throws ExecutionException, RemoteException;

    ExecutionResult<ModelObject> run0(ModelObject paramDataObject, ExecutionContext paramTransformContext)
            throws RemoteException;

    ModelObject createInputDataObject()
            throws RemoteException;

    ModelObject createOutputDataObject()
            throws RemoteException;

    List getOutputPorts()
            throws RemoteException;

    void setName(String name);

    String getName();
}
