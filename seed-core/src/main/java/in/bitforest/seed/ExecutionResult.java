package in.bitforest.seed;

import java.io.Serializable;
import java.util.List;

public class ExecutionResult<T> implements Serializable {
    private T output;
    private List<Exceptions> exceptions;

    public ExecutionResult(T output, List<Exceptions> exceptions) {
        this.exceptions = exceptions;
        this.output = output;
    }

    public T getOutput() {
        return (T) this.output;
    }

    public List<Exceptions> getExceptions() {
        return this.exceptions;
    }

    public void addExceptionsToList(List<Exceptions> addTo) {
        if (hasException()) {
            addTo.addAll(this.exceptions);
        }
    }

    public boolean hasException() {
        return (this.exceptions != null) && (this.exceptions.size() > 0);
    }
}
